from gym.envs.registration import register

register(
    id='Gomoku-custom-policy-v0',
    entry_point='gym_gomoku_custom_policy.envs:GomokuEnv',
    kwargs={
        'player_color': 'black',
        'opponent': 'custom',
        'board_size': 15,
    },
    nondeterministic=True,
)