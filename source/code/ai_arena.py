import re
import numpy as np
import tensorflow as tf
import os
import time
from ds_loader import DatasetLoader
from data_set import DataSet
from board import Board

from policy_based_ai import PolicyBasedAI
from human_ai import HumanAI


from ai_arena_game import Game

from time import gmtime, strftime

import random

                        
def Main():
    game = Game()

    ai_pool = []

    ai = PolicyBasedAI.Create(True, 'brain_strong')
    ai_pool.append(ai)

    ai = HumanAI.Create()
    ai_pool.append(ai)

    while True:

        ai0 = 0
        ai1 = 1
        print('select %d and %d to play' % (ai0, ai1))
        ai0 = ai_pool[ai0]
        ai1 = ai_pool[ai1]    
        game.MatchPK(ai1, ai0, 400, True)
        

if __name__ == '__main__':
    Main()
