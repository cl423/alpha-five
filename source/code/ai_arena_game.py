import re
import numpy as np
import tensorflow as tf
import os
import time
from ds_loader import DatasetLoader
from data_set import DataSet
from board import Board


from time import gmtime, strftime


class TrainDefs():
    STONE_EMPTY = 0
    STONE_BLACK = 1
    STONE_WHITE = 2

    NUM_CHANNELS = 3
    INPUT_SIZE = 15
    NUM_ACTIONS = INPUT_SIZE * INPUT_SIZE
    BOARD_SIZE_SQ = NUM_ACTIONS

    BATCH_SIZE = 32

    LEARNING_RATE = 0.001
    NUM_STEPS = 10000000
    DATASET_CAPACITY = 32 * 800

class TrainContext():
    
    ds_train = None
    ds_valid = None
    ds_test = None

    gstep = 0
    


class Game():
    def __init__(self):
        self.testvar = 10
        self.board = Board()
        self.board.set_board_size(TrainDefs.INPUT_SIZE)

    def Sample(self, ai1, ai2, moveRole):
        ai1WinCount = 1.0
        ai2WinCount = 1.0

        totalSampleCount = 1

        ais = []
        ais.append(ai1)
        ais.append(ai2)
        totalPlayers = len(ais)
        startTurn = 0
        if TrainDefs.STONE_BLACK == moveRole:
            startTurn = 0
        else:
            startTurn = 1

        for gameCount in range(totalSampleCount):
            sampleBoard = Board()
            sampleBoard.Clone(self.board)
            
            curTurn = startTurn      

            moveValid = True
            reChooseCount = 0
            for iter in range(20):            
            
                curPlayer = ais[curTurn]  
    
                move = curPlayer.GetMove(sampleBoard, curTurn, (moveValid == False))            

                if curTurn == 0:
                    move.stoneType = TrainDefs.STONE_BLACK
                else:
                    move.stoneType = TrainDefs.STONE_WHITE
 
                isWin, moveValid = sampleBoard.ApplyMove(move)

                if moveValid == False:
                    reChooseCount += 1
                    if reChooseCount > 100:
                        print('invalid move, total move count:%d.' % iter)
                        ai1WinCount += 1
                        ai2WinCount += 1
                        break

                    continue
                else:
                    reChooseCount = 0

                if isWin:
                    if curTurn == 0:
                        ai1WinCount += 1
                    else:
                        ai2WinCount += 1

                    break

                curTurn = (curTurn + 1) % totalPlayers

        winRate = 0.0
        if TrainDefs.STONE_BLACK == moveRole:
            winRate = float(ai1WinCount) / (ai1WinCount + ai2WinCount)
        else:
            winRate = float(ai2WinCount) / (ai1WinCount + ai2WinCount) 
                       
        return winRate

    def MatchPK(self, ai1, ai2, matchCount, showBoardEveryStep = False):
        # ai1 pk ai2
        # ai1 first move
        ais = []
        ais.append(ai1)
        ais.append(ai2)

        
        gameTotal = []
        allMoves = []

        curTurn = 0
        totalPlayers = len(ais)

        totalGameCount = 0
        ai1WinCount = 0
        ai2WinCount = 0
        moveValid = True
        reChooseCount = 0
        while totalGameCount < matchCount:            
            
            curPlayer = ais[curTurn] 
            if showBoardEveryStep:
                self.board.ShowBoard()    
            move = curPlayer.GetMove(self.board, curTurn, (moveValid == False))            

            if curTurn == 0:
                move.stoneType = TrainDefs.STONE_BLACK
            else:
                move.stoneType = TrainDefs.STONE_WHITE

            isWin = False
 
            isWin, moveValid = self.board.ApplyMove(move)
            print('ai %d choose to move to x:%d y:%d.' % (curTurn, move.x, move.y))
            
            if False == moveValid: 
                reChooseCount += 1
                if reChooseCount > 100:
                    print('invalid move, but we try again, reChooseCount:%d.' % reChooseCount)
                    allMoves = []
                    self.board.Clear()
                    curTurn = 0
                continue
            else:
                reChooseCount = 0

            allMoves.append((move.x, move.y))
            if isWin:
                totalGameCount += 1
                if curTurn == 0:
                    ai1WinCount += 1
                else:
                    ai2WinCount += 1

                print('totalGameCount:%d, ai1WinCount:%d, ai2WinCount:%d, moveCount:%d' % (totalGameCount, ai1WinCount, ai2WinCount, len(allMoves)))
                self.board.ShowBoard()
                # clear and restart
                gameTotal.append(allMoves)
                allMoves = []
                self.board.Clear()
                curTurn = 0

            curTurn = (curTurn + 1) % totalPlayers
