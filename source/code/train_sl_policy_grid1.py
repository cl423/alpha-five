import re
import numpy as np
import tensorflow as tf
import os
import time
import sys
from ds_loader import DatasetLoader
from data_set import DataSet
import matplotlib.pyplot as plt


class TrainDefs(object):
    STONE_EMPTY = 0
    STONE_BLACK = 1
    STONE_WHITE = 2

    NUM_CHANNELS = 3
    INPUT_SIZE = 15
    NUM_ACTIONS = INPUT_SIZE * INPUT_SIZE
    BOARD_SIZE_SQ = NUM_ACTIONS

    BATCH_SIZE = 32

    LEARNING_RATE = 0.001
    NUM_STEPS = 10000000
    DATASET_CAPACITY = 32 * 10


class TrainContext(object):
    
    ds_train = None
    ds_valid = None
    ds_test = None

    gstep = 0
    exit_epoch_count = 150


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.01)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def AdaptTrainData():
    h, w, c = TrainDefs.INPUT_SIZE, TrainDefs.INPUT_SIZE, TrainDefs.NUM_CHANNELS

    def f(dat):
        ds = []
        for row in dat:
            s, a = forge(row)
            ds.append((s, a))
        ds = np.array(ds)
        return DataSet(np.vstack(ds[:, 0]).reshape((-1, h, w, c)), np.vstack(ds[:, 1]))

    if TrainContext.ds_train is None:
        ds_train, TrainContext._has_more_data = TrainContext.loader_train.load(TrainDefs.DATASET_CAPACITY)
        TrainContext.ds_train = f(ds_train)

    if TrainContext.ds_valid is None:
        ds_valid, _ = TrainContext.loader_valid.load(TrainDefs.DATASET_CAPACITY // 2)
        TrainContext.ds_valid = f(ds_valid)

    if TrainContext.ds_test is None:
        ds_test, _ = TrainContext.loader_test.load(TrainDefs.DATASET_CAPACITY // 2)
        TrainContext.ds_test = f(ds_test)

    print('Train dataset shape:', TrainContext.ds_train.images.shape, TrainContext.ds_train.labels.shape)
    print('Valid dataset shape:', TrainContext.ds_valid.images.shape, TrainContext.ds_valid.labels.shape)
    print('Test dataset shape:', TrainContext.ds_test.images.shape, TrainContext.ds_test.labels.shape)


def adapt_state(board):
    black = (board == TrainDefs.STONE_BLACK).astype(float)
    white = (board == TrainDefs.STONE_WHITE).astype(float)
    empty = (board == TrainDefs.STONE_EMPTY).astype(float)

    # switch perspective
    bn = np.count_nonzero(black)
    wn = np.count_nonzero(white)
    if bn != wn:  # if it is white turn, swith it
        black, white = white, black

    image = np.dstack((black, white, empty)).ravel()
    legal = empty.astype(bool)
    return image, legal


def forge(row):
    board = row[:TrainDefs.BOARD_SIZE_SQ]
    image, _ = adapt_state(board)

    visit = row[TrainDefs.BOARD_SIZE_SQ::2]

    win_rate = visit
    s = np.sum(win_rate)
    win_rate /= s
    return image, win_rate


def create_conv_net(states_pl):
    ch1 = 32
    W_1 = weight_variable([3, 3, TrainDefs.NUM_CHANNELS, ch1])
    b_1 = bias_variable([ch1])

    ch = 32
    W_2 = weight_variable([3, 3, ch1, ch])
    b_2 = bias_variable([ch])
    W_21 = weight_variable([3, 3, ch, ch])
    b_21 = bias_variable([ch])
    W_22 = weight_variable([3, 3, ch, ch])
    b_22 = bias_variable([ch])
    W_23 = weight_variable([3, 3, ch, ch])
    b_23 = bias_variable([ch])
    W_24 = weight_variable([3, 3, ch, ch])
    b_24 = bias_variable([ch])
    W_25 = weight_variable([3, 3, ch, ch])
    b_25 = bias_variable([ch])
    W_26 = weight_variable([1, 1, ch, ch])
    b_26 = bias_variable([ch])
    W_27 = weight_variable([3, 3, ch, ch])
    b_27 = bias_variable([ch])
    W_28 = weight_variable([1, 1, ch, ch])
    b_28 = bias_variable([ch])

    W_29 = weight_variable([1, 1, ch, 1024])
    b_29 = bias_variable([1024])

    h_conv1 = tf.nn.relu(tf.nn.conv2d(states_pl, W_1, [1, 1, 1, 1], padding='SAME') + b_1)
    # h_conv1 = tf.layers.batch_normalization(h_conv1)
    h_conv2 = tf.nn.relu(tf.nn.conv2d(h_conv1, W_2, [1, 1, 1, 1], padding='SAME') + b_2)
    # h_conv2 = tf.layers.batch_normalization(h_conv2)
    h_conv21 = tf.nn.relu(tf.nn.conv2d(h_conv2, W_21, [1, 1, 1, 1], padding='SAME') + b_21)
    # h_conv21 = tf.layers.batch_normalization(h_conv21)
    h_conv22 = tf.nn.relu(tf.nn.conv2d(h_conv21, W_22, [1, 1, 1, 1], padding='SAME') + b_22)
    # h_conv22 = tf.layers.batch_normalization(h_conv22)
    h_conv23 = tf.nn.relu(tf.nn.conv2d(h_conv22, W_23, [1, 1, 1, 1], padding='SAME') + b_23)
    # h_conv23 = tf.layers.batch_normalization(h_conv23)
    h_conv24 = tf.nn.relu(tf.nn.conv2d(h_conv23, W_24, [1, 1, 1, 1], padding='SAME') + b_24)
    # h_conv24 = tf.layers.batch_normalization(h_conv24)
    h_conv25 = tf.nn.relu(tf.nn.conv2d(h_conv24, W_25, [1, 1, 1, 1], padding='SAME') + b_25)
    # h_conv25 = tf.layers.batch_normalization(h_conv25)
    h_conv26 = tf.nn.relu(tf.nn.conv2d(h_conv25, W_26, [1, 1, 1, 1], padding='SAME') + b_26)
    # h_conv26 = tf.layers.batch_normalization(h_conv26)
    h_conv27 = tf.nn.relu(tf.nn.conv2d(h_conv26, W_27, [1, 1, 1, 1], padding='SAME') + b_27)
    # h_conv27 = tf.layers.batch_normalization(h_conv27)
    h_conv28 = tf.nn.relu(tf.nn.conv2d(h_conv27, W_28, [1, 1, 1, 1], padding='SAME') + b_28)
    # h_conv28 = tf.layers.batch_normalization(h_conv28)
    h_conv29 = tf.nn.relu(tf.nn.conv2d(h_conv28, W_29, [1, 1, 1, 1], padding='SAME') + b_29)
    # h_conv29 = tf.layers.batch_normalization(h_conv29)

    conv_out_dim = h_conv29.get_shape()[1:].num_elements()
    conv_out = tf.reshape(h_conv29, [-1, conv_out_dim])
    return conv_out, conv_out_dim


def create_policy_net(states_pl):
    conv, conv_out_dim = create_conv_net(states_pl)
    conv = tf.identity(conv, 'policy_net_conv')
    W_3 = weight_variable([conv_out_dim, TrainDefs.INPUT_SIZE * TrainDefs.INPUT_SIZE])
    b_3 = bias_variable([TrainDefs.INPUT_SIZE * TrainDefs.INPUT_SIZE])

    fc_out = tf.matmul(conv, W_3) + b_3
    return fc_out


def CreateModel(states_pl, actions_pl):
    predictions = None
    with tf.variable_scope('policy_net'):
        predictions = create_policy_net(states_pl)

    policy_net_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)

    pg_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=predictions, labels=actions_pl))
    reg_loss = tf.reduce_sum([tf.reduce_sum(tf.square(x)) for x in policy_net_vars])
    loss = pg_loss  + 0.001 * reg_loss
    log_loss=tf.summary.scalar('loss', loss)

    optimizer = tf.train.AdamOptimizer(0.0001)
    opt_op = optimizer.minimize(loss)

    predict_probs = tf.nn.softmax(predictions)
    eq = tf.equal(tf.argmax(predict_probs, 1), tf.argmax(actions_pl, 1))
    TrainContext.eval_correct = tf.reduce_sum(tf.cast(eq, tf.int32))

    return opt_op, loss, predict_probs


def LoadDataSet(dataFileName):
    this_script_path = TrainContext.data_path
    dataFileFullName = "%s\\..\\..\\data\\train_policy\\%s" % (this_script_path, dataFileName)
    print("Load data from file:" + dataFileFullName)
    
    return DatasetLoader(dataFileFullName)


def fill_feed_dict(data_set, states_pl, actions_pl, batch_size=None):
    batch_size = batch_size or TrainDefs.BATCH_SIZE
    states_feed, actions_feed = data_set.next_batch(batch_size)
    feed_dict = {
        states_pl: states_feed,
        actions_pl: actions_feed
    }
    return feed_dict


def DoEval(sess, eval_correct, states_pl, actions_pl, data_set):
    true_count = 0
    batch_size = TrainDefs.BATCH_SIZE
    steps_per_epoch = data_set.num_examples // batch_size
    num_examples = steps_per_epoch * batch_size
    for _ in range(steps_per_epoch):
        feed_dict = fill_feed_dict(data_set, states_pl, actions_pl, batch_size)
        true_count += sess.run(eval_correct, feed_dict=feed_dict)
    precision = true_count / (num_examples or 1)
    return precision


def Train(sess, states, actions, opt_op, loss, saver, train_writer, epoch, ta, va):
    print('Now we start training one epoch.')

    NUM_STEPS = TrainContext.ds_train.num_examples // TrainDefs.BATCH_SIZE
    print('total num steps:', NUM_STEPS)

    start_time = time.time()
    train_accuracy = 0
    validation_accuracy = 0
    merged = tf.summary.merge_all()
    for step in range(NUM_STEPS):
        feed_dict = fill_feed_dict(TrainContext.ds_train, states, actions)
        summary, _, lossVal = sess.run([merged, opt_op, loss], feed_dict=feed_dict)
        print('loss value is:%f' % lossVal)
        TrainContext.gstep += 1

        if step == NUM_STEPS - 1:
            train_writer.add_summary(summary, epoch)
            saver.save(sess, TrainContext.BRAIN_CHECKPOINT_FILE, global_step=TrainContext.gstep)
            train_accuracy = DoEval(sess, TrainContext.eval_correct, states, actions, TrainContext.ds_train)
            validation_accuracy = DoEval(sess, TrainContext.eval_correct, states, actions, TrainContext.ds_valid)

    duration = time.time() - start_time
    test_accuracy = DoEval(sess, TrainContext.eval_correct, states, actions, TrainContext.ds_test)
    print('acc_train: %.3f, acc_valid: %.3f, test accuracy: %.3f, time cost: %.3f sec' %
            (train_accuracy, validation_accuracy, test_accuracy, duration))
    ta.append(train_accuracy)
    va.append(validation_accuracy)
    return False


def LoadTrainData():
    # Load training set and validate set
    TrainContext.loader_train = LoadDataSet("train.txt")
    TrainContext.loader_valid = LoadDataSet("valid.txt")
    TrainContext.loader_test = LoadDataSet("test.txt")


def InitFilePath(brainSubFolder = 'brain'):
    if not hasattr(TrainContext, 'data_path'):
        TrainContext.data_path = os.path.split(os.path.realpath(__file__))[0]
    this_script_path = TrainContext.data_path
    
    TrainContext.BRAIN_DIR = "%s\\..\\..\\data\\%s\\" % (this_script_path, brainSubFolder)
    if not os.path.exists(TrainContext.BRAIN_DIR):
        os.makedirs(TrainContext.BRAIN_DIR)

    TrainContext.BRAIN_CHECKPOINT_FILE = "%s\\..\\..\\data\\%s\\%s" % (this_script_path, brainSubFolder, 'model.ckpt')


def LoadFromExist(sess, saver):
    ckpt = tf.train.get_checkpoint_state(TrainContext.BRAIN_DIR)
    if ckpt and ckpt.model_checkpoint_path:
        saver.restore(sess, ckpt.model_checkpoint_path)
        arr = re.split('-|model\.ckpt', ckpt.model_checkpoint_path)
        TrainContext.gstep = 0
                    

def Main(loadExist = False):

    LoadTrainData()

    InitFilePath('brain_grid1')

    # Create operations and tensors on default graph
    states = tf.placeholder(tf.float32, [None, 15, 15, 3])

    actions = tf.placeholder(tf.float32, [None, 225])

    # define model
    opt_op, loss, prediction = CreateModel(states, actions)

    # create saver object
    saver = tf.train.Saver(tf.trainable_variables())

    print("makrdir")
    if tf.gfile.Exists('C:\\Users\\Evan\\PycharmProjects\\alpha-five\\source\\code\\logs'):
        tf.gfile.DeleteRecursively('C:\\Users\\Evan\\PycharmProjects\\alpha-five\\source\\code\\logs')
    tf.gfile.MakeDirs('C:\\Users\\Evan\\PycharmProjects\\alpha-five\\source\\code\\logs\\train')

    # Initialize tensorflow 
    init = tf.initialize_all_variables()
    sess = tf.Session()
    sess.run(init)

    train_writer = tf.summary.FileWriter('C:\\Users\\Evan\\PycharmProjects\\alpha-five\\source\\code\\logs\\train', sess.graph)


    if loadExist:
        # load from saved chpt file
        LoadFromExist(sess, saver)
        print('Load finished.')

    # run training
    epoch = 0
    ta = []
    va = []
    while True:
        # do the epoch thing inside.
        epoch += 1
        print('epoch count:%d, exit epoch:%d' % (epoch, TrainContext.exit_epoch_count))

        AdaptTrainData()         
        Train(sess, states, actions, opt_op, loss, saver, train_writer, epoch, ta, va)
        print(ta)
        print(va)
        if epoch >= TrainContext.exit_epoch_count:
            break
    train_writer.close()
    plt.plot(range(50), ta*48, label='validation_accuracy')
    # plt.plot(range(50), va, label='validation_accuracy')
    plt.xlabel("epoch")
    plt.ylabel("accuracy")
    plt.legend()
    plt.show()

if __name__ == '__main__':
    TrainContext.data_path = os.path.split(os.path.realpath(__file__))[0] 
    if len(sys.argv) >= 2:
        TrainContext.data_path = sys.argv[1]

    if len(sys.argv) >= 3:
        TrainContext.exit_epoch_count = int(sys.argv[2])

    Main(loadExist=True)



