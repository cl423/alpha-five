import re
import numpy as np
import tensorflow as tf
import os
import time
from ds_loader import DatasetLoader
from data_set import DataSet
from board import Board

from time import gmtime, strftime

from train_sl_policy_grid1 import CreateModel
from train_sl_policy_grid1 import LoadFromExist
from train_sl_policy_grid1 import InitFilePath


class PolicyBasedAI():
    @staticmethod
    def Create(isRandom, brainSubFolder = 'brain'):
        InitFilePath(brainSubFolder)
        newGraph = tf.Graph()   
        with newGraph.as_default():
            # Create operations and tensors on default graph
            states = tf.placeholder(tf.float32, [None, 15, 15, 3])

            actions = tf.placeholder(tf.int64, [None, 225])

            # define model
            opt_op, loss, predict = CreateModel(states, actions)

            # create saver object
            saver = tf.train.Saver(tf.trainable_variables())

            # Initialize tensorflow 
            config = tf.ConfigProto(
                    device_count={'GPU': 0}
                )

            init = tf.global_variables_initializer()
            sess = tf.Session(config=config, graph=newGraph)
            sess.run(init)

            ai = PolicyBasedAI(sess, newGraph, states, predict) 
            ai.isRandom = isRandom

            # load from saved chpt file
            LoadFromExist(sess, saver) 

        return ai

    def __init__(self, session, graph, state, predict):
        self.sess = session
        self.graph = graph
        self.predict_probs = predict
        self.state = state

    def get_move_probs(self, state):
        feed_dict = {
            self.state: state,
        }

        return self.sess.run(self.predict_probs, feed_dict=feed_dict)

    def GetMove(self, board, turn, reChoose=False):
        state = board.GetState()
        moveChoice = self.get_move_probs(state)
        best_move = np.argmax(moveChoice, 1)[0]
        print('++++++++++++++++++++++++++++')
        topK = moveChoice[0].argsort()[-5:][::-1]
        for betterMove in topK:
            loc = np.unravel_index(betterMove, (Board.BOARD_SIZE, Board.BOARD_SIZE))
            print('candidate move:(%d, %d), rand:%f' % (loc[0], loc[1], moveChoice[0][betterMove]))
        print('----------------------------')
        if self.isRandom:            
            K = 2
            epslon = 0.1

            if reChoose:
                print('reChoose, K = 4, epslon = 10')
                K = 4
                epslon = 10
            print('++++++++++++++++++++++++++++')
            topK = moveChoice[0].argsort()[-K:][::-1]
            for betterMove in topK:
                loc = np.unravel_index(betterMove, (Board.BOARD_SIZE, Board.BOARD_SIZE))
                print('candidate move:(%d, %d)' % (loc[0], loc[1]))
            print('----------------------------')
            stdDev = np.std(moveChoice[0][topK])
            stdDev = stdDev / np.mean(moveChoice[0][topK])
            if stdDev < epslon:
                alterMove = topK[np.random.choice(K)]
                if alterMove != best_move:
                    print('best_move:%d, alterMove1:%d' % (best_move, alterMove))
                    best_move = alterMove

        loc = np.unravel_index(best_move, (Board.BOARD_SIZE, Board.BOARD_SIZE))
        choosedMove = type('', (), {})()
        choosedMove.x = loc[0]
        choosedMove.y = loc[1]

        return choosedMove
