import re
import numpy as np
import tensorflow as tf
import os,sys
import time
from ds_loader import DatasetLoader
from data_set import DataSet
from board import Board


from time import gmtime, strftime


class HumanAI():
    @staticmethod
    def Create():
        ai = HumanAI()
        return ai

    def __init__(self):
        a = 10

    def get_move_probs(self, state):
        
        feed_dict = {
            self.state: state,
        }

        return self.sess.run(self.predict_probs, feed_dict=feed_dict)

    def GetMove(self, board, turn, reChoose=False):
        state = board.GetState()
        print('please input move:')
        raw_loc = sys.stdin.readline()
        raw_loc = raw_loc.split(' ')
        loc = []
        loc.append(int(raw_loc[0]))
        loc.append(ord(raw_loc[1][0])-ord('A'))

        choosedMove = type('', (), {})()
        choosedMove.x = loc[0]
        choosedMove.y = loc[1]

        return choosedMove